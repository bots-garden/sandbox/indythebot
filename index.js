const {Service, DiscoveryBackend, fetch} = require('./pico')
const url = require("url")

let port = process.env.PORT || 8080;
let externalPort = process.env.EXTERNAL_PORT || port;

let service = new Service({})

function make_base_auth() {
  let token = process.env.BOT_CHAT_TOKEN
  let user = process.env.BOT_NAME 
  return "Basic " + Buffer.from(`${token}:${user}`).toString('base64');
}

service.post({uri:`/hey`, f: (request, response) => {
  let message = JSON.stringify(request.body)
  let serviceurl = url.parse(process.env.CHAT_SERVER_URL)

  fetch({
    protocol: serviceurl.protocol.slice(0, -1), 
    host: serviceurl.hostname, 
    port: serviceurl.port,
    path: `/rooms/${process.env.ROOM_NAME}/messages`,
    method: 'POST',
    data:JSON.stringify({text: message}),
    headers:  {
      "Content-Type": "application/json; charset=utf-8",
      "Authorization": make_base_auth()
    }
  }).then(data => { // registration ok      
    console.log(data)
  }).catch(err => { // registration ko      
    console.log(err)
  })

  response.sendJson({message: "👋", from:"pico"})
}})



service.get({uri:`/`, f: (request, response) => {
  response.sendHtml(`
    <!doctype html>
    <html>
      <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Indy The Bot</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <style>
            .container { min-height: 100vh; display: flex; justify-content: center; align-items: center; text-align: center; }
            .title { font-family: "Source Sans Pro", "Helvetica Neue", Arial, sans-serif; display: block; font-weight: 300; font-size: 100px; color: #35495e; letter-spacing: 1px; }
            .subtitle { font-family: "Source Sans Pro", "Helvetica Neue", Arial, sans-serif; font-weight: 300; font-size: 42px; color: #526488; word-spacing: 5px; padding-bottom: 15px; }
            .links { padding-top: 15px; }
        </style>
      </head>
      <body>
        <section class="container">
          <div>
            <h1 class="title">
              Indy the 🤖 
            </h1>
            <h2 class="subtitle">
              made with ❤️
            </h2>
            <h2 class="subtitle">
              built with GitLab CI/CD and GitLab Runner 
            </h2>               
          </div>
        </section>
      </body>
    </html>  
  `)

}})

service.start({port: port}, res => {
  res.when({
    Failure: error => console.log("😡 Houston? We have a problem!", error),
    Success: port => console.log(`🌍 pico service is listening on ${port}`)
  })
})
